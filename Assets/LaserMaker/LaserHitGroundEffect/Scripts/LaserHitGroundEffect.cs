﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace LaserMaker
{
    public class LaserHitGroundEffect : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            HitEffectReturnToPool();
        }

        [SerializeField]
        private LaserHitGroundEffectPool laserHitGroundEffectPool;
        private float _timer = 0;
        public float lifeTime = 30.0f;

        private void Awake()
        {
            laserHitGroundEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LaserHitGroundEffectPool>();
        }

        private void OnEnable()
        {
            _timer = Time.time;
        }

        public void HitEffectReturnToPool()
        {
            if (!gameObject.activeInHierarchy) { return; }

            if (Time.time > _timer + lifeTime)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
            {
                laserHitGroundEffectPool.Recovery(this.gameObject);
            }

        }
    }
}
