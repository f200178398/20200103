﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SA
{
    public class BulletFire : MonoBehaviour
    {
        public BulletFirePool bulletFirePool;
        [Header("特效的生命")]
        public float _timer;
        public float bulletFireLife=1.0f;
        // Start is called before the first frame update
        void Start()
        {
            bulletFirePool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<BulletFirePool>();
            bulletFireLife = 1.0f;
        }

        // Update is called once per frame
        void Update()
        {
            BulletFireLife();
        }
        private void OnEnable()
        {
            _timer = Time.time;
        }

        private void BulletFireLife()
        {
            if (!gameObject.activeInHierarchy)
                return;

            if (Time.time > _timer + bulletFireLife)
            {
                bulletFirePool.Recovery(this.gameObject);
            }
        }
    }
}