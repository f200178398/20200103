﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMain_1 : MonoBehaviour
{
    
    private void Awake()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        BulletLife();
        BulletMove();
    }

    [Header("Bullet Settings")]
    public float bulletLife=5.0f;
    public float bulletSpeed = 10.0f;

    public void BulletLife()
    {
        bulletLife -= Time.deltaTime;
        if(bulletLife <0) { Destroy(this.gameObject); }
    }
    public void BulletMove()
    {
        //直直地往前射
        this.transform.position += this.transform.forward * bulletSpeed * Time.deltaTime;

        //會自動找尋目標，對著目標射
        //this.transform.position +=  (Sphere1.transform.position-this.transform.position)* /*這邊控制子彈的速度*/bulletSpeed * Time.deltaTime;
        
        
        //會抓取滑鼠的x和y的兩個方向的向量，不會有z方向的向量。所以射出來會有點奇怪
        //this.transform.position+= (Input.mousePosition-transform.position).normalized* bulletSpeed * Time.deltaTime;
        //因為子彈是capsole形狀的，所以沿著的方向是up
    }


    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Bullet"))//子彈打到子彈圖層的時候不做任何事
        {
            //Destroy(this.gameObject);
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("Bubble"))//子彈打到泡泡圖層會把泡泡打爆。子彈自己也消失
        {
            Destroy(this.gameObject);
            Destroy(other.gameObject);
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("PostProcessing"))//子彈打到非主角圖層的東西會爆炸=子彈打到主角不會有事
        {
            //Destroy(this.gameObject);
        }    
    }
   


    private void OnDrawGizmos()
    {
        //這邊畫子彈的forward(青色), right(綠色), up(粉色)
        //Gizmos.color = Color.cyan;
        //Gizmos.DrawLine(this.transform.position, this.transform.position+this.transform.forward);
        //Gizmos.color = Color.green;
        //Gizmos.DrawLine(this.transform.position, this.transform.position+this.transform.right);
        //Gizmos.color = Color.magenta;
        //Gizmos.DrawLine(this.transform.position, this.transform.position + this.transform.up);
    }

}
