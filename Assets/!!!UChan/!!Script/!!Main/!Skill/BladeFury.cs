﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;
public class BladeFury : MonoBehaviour
{
    
    public float baldeSpeed = 60f;
    public BladeFuryPool pool;
    // Start is called before the first frame update
    void Start()
    {
        
        pool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<BladeFuryPool>();
    }

    // Update is called once per frame
    void Update()
    {
        UseBladeFury();
        BladeFurryLifeReturnToPool();
    }

    public float startTime;
    public float bladeFuryLife = 1f;

    private void OnEnable()
    {
        startTime += Time.time;
    }
    void UseBladeFury()
    {        
        
        //if(attackEvent.isBladeFury == true)
        //{           
            this.transform.position += this.transform.forward * baldeSpeed * Time.deltaTime;          
                //attackEvent.isBladeFury = false;                          
        //}               
    }

    void BladeFurryLifeReturnToPool()
    {
        if (Time.time > startTime + bladeFuryLife)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
        {
            pool.Recovery(this.gameObject);
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        //if(other.gameObject.tag == ("Enemy"))
        //{
        //    pool.Recovery(this.gameObject);
        //}
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, Vector3.forward * 10f);
    }
}
