﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceToCamera : MonoBehaviour
{
    Camera cameraMain;
    [Header("天堂路的相機")]
    public Camera heavenRoadCamera;
    public TimelineManager tm_AirShipAppeared;
    public TimelineManager tm_BossAppeared;
    // Start is called before the first frame update
    void Start()
    {
        cameraMain = Camera.main;
        heavenRoadCamera = GameObject.FindGameObjectWithTag("heavenRoadCamera").GetComponent<Camera>();
        tm_AirShipAppeared = GameObject.Find("AnimEvent_04_飛船登場_Tri").GetComponent<TimelineManager>();
        tm_BossAppeared = GameObject.Find("AnimEvent_05_boss登場Tri").GetComponent<TimelineManager>();
    }

    
    void Update()
    {     
         if (tm_AirShipAppeared.b_hasBeenTiggered && !tm_BossAppeared.b_hasBeenTiggered)
        {
            transform.LookAt(transform.position + heavenRoadCamera.transform.rotation * Vector3.forward,
                heavenRoadCamera.transform.rotation * Vector3.up);
        }
        else 
        {
            transform.LookAt(transform.position + cameraMain.transform.rotation * Vector3.forward,
            cameraMain.transform.rotation * Vector3.up);
        }
    }
}
