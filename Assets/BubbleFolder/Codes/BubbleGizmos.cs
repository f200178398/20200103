﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleGizmos : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(0, 0, 255);
        Gizmos.DrawLine(this.transform.position, this.transform.position + this.transform.forward*3);
    }
}
