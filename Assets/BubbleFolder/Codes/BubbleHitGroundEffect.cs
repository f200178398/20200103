﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bubble;

public class BubbleHitGroundEffect : MonoBehaviour
{

    public BubbleHitGroundEffectPool bubbleHitGroundEffectPool;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        bubbleHitGroundEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<BubbleHitGroundEffectPool>();
        effectLife = 2f;
    }
    private void OnEnable()
    {
        _timer = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        EffectLifeReturnToPool();
    }

    public float _timer;
    public float effectLife;

    void EffectLifeReturnToPool()//新版的回到物件池的泡泡
    {
        //下面這段if，應該只是防呆，有測試過如果刪掉還是一樣可以執行
        if (!gameObject.activeInHierarchy)
            return;

        if (Time.time > _timer + effectLife)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
        {
            bubbleHitGroundEffectPool.Recovery(this.gameObject);
        }
    }
}
