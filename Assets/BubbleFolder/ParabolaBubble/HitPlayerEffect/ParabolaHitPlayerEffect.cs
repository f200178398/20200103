﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParabolaHitPlayerEffect : MonoBehaviour
{
    public ParabolaHitPlayerEffectPool parabolaHitPlayerEffectPool;
    [SerializeField]
    private float _timer = 0;
    public float lifeTime = 3.0f;
    // Start is called before the first frame update
    void Awake()
    {
        parabolaHitPlayerEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<ParabolaHitPlayerEffectPool>();
    }

    // Update is called once per frame
    void Update()
    {
        ReturnToEffectPool();
    }
    private void OnEnable()
    {
        _timer = Time.time;
    }

    public void ReturnToEffectPool()
    {
        if (!gameObject.activeInHierarchy) { return; }

        if (Time.time > _timer + lifeTime)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
        {
            parabolaHitPlayerEffectPool.Recovery(this.gameObject);
        }
    }
}
