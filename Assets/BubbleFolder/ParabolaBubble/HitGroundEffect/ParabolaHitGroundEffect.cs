﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParabolaHitGroundEffect : MonoBehaviour
{
    public ParabolaHitGroundEffectPool parabolaHitGroundEffectPool;
    [SerializeField]
    private float _timer = 0;
    public float lifeTime = 3.0f;
    // Start is called before the first frame update
    void Awake()
    {
        parabolaHitGroundEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<ParabolaHitGroundEffectPool>();
    }

    // Update is called once per frame
    void Update()
    {
        ReturnToEffectPool();
    }
    

    private void OnEnable()
    {
        _timer = Time.time;
    }

    public void ReturnToEffectPool()
    {
        if (!gameObject.activeInHierarchy) { return; }

        if (Time.time > _timer + lifeTime)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
        {
            parabolaHitGroundEffectPool.Recovery(this.gameObject);
        }
    }
}
